<?php

namespace Webkul\Product\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCityFee extends Model
{
    protected $fillable = ['product_id', 'city_id', 'custom_price'];

    protected $table = 'products_cities_fee';

    public $timestamps = false;
}