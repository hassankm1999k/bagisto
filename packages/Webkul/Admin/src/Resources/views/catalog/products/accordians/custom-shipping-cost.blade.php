<table class="table">
    <thead>
        <tr>
            <th class="grid_head">#</th>
            <th class="grid_head">City</th>
            <th class="grid_head">Custom Shipping Cost</th>
        </tr>
    </thead>
    <tbody class="text-right">

        @foreach($cities as $city)
            {{ $selected = false }}
            @foreach($cityFees as $cityFee) @if($cityFee->city_id === $city->id)<p class="hide">{{$selected = true}} {{ $currentCityFee = $cityFee->custom_price }}</p>   @endif @endforeach
            <tr style="align-items: center;text-align: center">
                <td>
                    <input name="city_fees_checkboxes[]"
                           type="checkbox"
                           value="{{json_encode(['city_id' => $city->id,'index' => $loop->index])}}"
                           @if($selected === true )
                                checked
                           @endif
                    >

                </td>
                <td>{{ $city->name }}</td>
                <td>
                    <div class="control-group text" style="width: 50%; margin-left: 25%">
                         <input
                             step="any"
                             type="number"
                             id="city_fees"
                             name="city_fees[]"
                             class="control"
                             @if($selected === true )
                                value="{{ $currentCityFee }}"
                             @else
                                 value="0.00"
                             @endif
                         >
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>