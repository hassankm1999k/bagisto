<?php

return [
    'flatrate' => [
        'code'             => 'flatrate',
        'title'            => 'Flat Rate',
        'description'      => 'Flat Rate Shipping',
        'active'           => true,
        'is_calculate_tax' => true,
        'default_rate'     => '10',
        'type'             => 'per_unit',
        'class'            => 'Webkul\Shipping\Carriers\FlatRate',
    ],

    'free'     => [
        'code'             => 'free',
        'title'            => 'Free Shipping',
        'description'      => 'Free Shipping',
        'active'           => true,
        'is_calculate_tax' => true,
        'default_rate'     => '0',
        'class'            => 'Webkul\Shipping\Carriers\Free',
    ],
    'CustomShippingMethod' => [
        'code' => 'custom-shipping-method',
        'title' => 'Custom Shipping Method',
        'description'      => 'Ship By your City address.',
        'active'           => true,
        'is_calculate_tax' => true,
        'default_rate'     => '0',
        'class'            => 'Webkul\Shipping\Carriers\CustomShippingMethod',
    ]
];