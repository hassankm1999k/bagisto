<?php

namespace Webkul\Shipping\Carriers;

use ACME\City\Models\City;
use Illuminate\Support\Facades\DB;
use Webkul\Checkout\Facades\Cart;
use Webkul\Checkout\Models\CartShippingRate;
use Webkul\Core\Models\Address;
use Webkul\Product\Models\ProductCityFee;

class CustomShippingMethod extends AbstractShipping
{

    /**
     * Payment method code
     *
     * @var string
     */
    protected $code  = 'CustomShippingMethod';

    public function calculate()
    {
        if (! $this->isAvailable()) {
            return false;
        }

        $cart = Cart::getCart();

        $object = new CartShippingRate;

        $object->carrier = 'CustomShippingMethod';
        $object->carrier_title = $this->getConfigData('title');
        $object->method = 'custom-shipping-method';
        $object->method_title = $this->getConfigData('title');
        $object->method_description = $this->getConfigData('description');
        $object->is_calculate_tax = $this->getConfigData('is_calculate_tax');
        $object->price = 0;
        $object->base_price = 0;

        $address = DB::table('addresses')
            ->where('address_type', '=', 'cart_shipping')
            ->where('cart_id', $cart->id)->first();

//        $address = Address::where('cart_id', $cart->id)
//            ->where('address_type', '=', 'cart_shipping')
//            ->first();

        $city = City::where('name', '=', $address->city)->first();
        foreach ($cart->items as $item) {
            $productId = $item->product->id;
            if ($city)
                $cityFee = ProductCityFee::where('product_id', $productId)
                    ->where('city_id', $city->id)->first();

            $amount = 0;
            if (isset($cityFee))
                $amount = $cityFee->custom_price;

            $object->price += core()->convertPrice($amount) * $item->quantity;
            $object->base_price += $amount * $item->quantity;
        }

        return $object;
    }
}