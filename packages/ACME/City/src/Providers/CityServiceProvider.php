<?php

namespace ACME\City\Providers;

use ACME\City\View\Components\Form;
use ACME\City\View\Components\Text;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

/**
 * HelloWorld service provider
 *
 * @author    Jane Doe <janedoe@gmail.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CityServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/../Http/routes.php';
        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'city');
        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'city');

        $this->loadMigrationsFrom(__DIR__ .'/../Database/Migrations');

        Event::listen('bagisto.admin.layout.head', function($viewRenderEventManager) {
            $viewRenderEventManager->addTemplate('city::layouts.style');
        });

        Blade::component('text', Text::class);
        Blade::component('form', Form::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/menu.php', 'menu.admin'
        );

        require_once(dirname(__DIR__). '/Helpers/AdminHelper.php');
        require_once(dirname(__DIR__). '/Helpers/StringHelper.php');
    }
}