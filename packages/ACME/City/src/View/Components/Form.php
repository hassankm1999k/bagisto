<?php

namespace ACME\City\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class Form extends Component
{
    /**
     * The Attr action.
     *
     * @var string
     */
    public $action;

    /**
     * The Attr method.
     *
     * @var string
     */
    public $method;

    /**
     * The Attr submit.
     *
     * @var string
     */
    public $submit;

    /**
     * The Attr entity.
     *
     */
    public $entity;

    /**
     * Create a new component instance.
     *
     * @param string $action
     * @param string $method
     * @param string $submit
     * @param null $entity
     */
    public function __construct($action = '', $method = 'POST', $submit = 'POST', $entity = null)
    {
        $this->action = $action;
        $this->method = $method;
        $this->submit = $submit;
        $this->entity = $entity;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('city::components.form');
    }
}
