<?php

namespace ACME\City\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class BaseComponent extends Component
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var boolean
     */
    public $required;

    /**
     * The Attr oldValue.
     *
     */
    public $oldValue;

    /**
     * @var string
     */
    public $valueName;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param bool $required
     * @param null $oldValue
     * @param string $valueName
     */
    public function __construct($name = '', $required = false, $oldValue=null, $valueName='')
    {
        $this->name = $name;
        $this->required = $required;
        $this->oldValue = $oldValue;
        if ($valueName == '')
            $this->valueName = $this->name;
        else
            $this->valueName = $valueName;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
//        return view('Components.text-area');
    }
}
