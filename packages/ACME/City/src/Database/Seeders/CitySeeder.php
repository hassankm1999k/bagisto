<?php

namespace ACME\City\Database\Seeders;

use ACME\City\Models\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $cities = ['Aswan', 'Cairo', 'Iskandria', 'Maadi'];
        foreach ($cities as $city){
            City::create([
                'name' => $city,
                'country' => 'Egypt'
            ]);
        }
    }
}