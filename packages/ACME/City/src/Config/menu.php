<?php

return [
    [
        'key' => 'city',
        'name' => 'Cities',
        'route' => 'admin.city.index',
        'sort' => 9,
        'icon-class' => 'list-icon',
    ]
];