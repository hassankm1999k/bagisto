<?php

use Illuminate\Support\Facades\Route;

function getAction(): ?string
{
    return explode("@", Route::currentRouteAction())[1] ?? "";
}

function getActionMethod(): ?string
{
    $currentAction = explode("@", Route::currentRouteAction())[1];
    if ($currentAction == 'create')
        return 'POST';
    else
        return 'PUT';
}

function getNextAction($item, $entity = null): string
{
    $currentAction = explode("@", Route::currentRouteAction())[1];
    if ($currentAction == 'create')
        return route('admin.' . $item . '.store');
    else{

        return route('admin.' . $item .'.update', [strval($item) => $entity]);
    }

}

