<?php

use Illuminate\Support\Str;

function camel($string = '')
{
    return Str::camel($string);
}

function plural($string)
{
    return Str::plural($string);
}

function toTitle($string = '')
{
    return Str::of($string)->replace('_', ' ')->replaceMatches('/\d+/u',' ')->ucfirst();
}

