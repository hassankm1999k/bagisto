<?php

namespace ACME\City\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class CityDataGrid extends DataGrid
{
    /**
     * Index.
     *
     * @var string
     */
    protected $index = 'city_id';

    /**
     * Sort order.
     *
     * @var string
     */
    protected $sortOrder = 'asc';

    /**
     * Items per page.
     *
     * @var int
     */
    protected $itemsPerPage = 10;

    /**
     * Prepare query builder.
     *
     * @return void
     */
    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('cities')
            ->select('cities.id as city_id', 'cities.name', 'cities.country');

        $this->addFilter('city_id', 'cities.id');
        $this->addFilter('name', 'cities.name');
        $this->addFilter('country', 'cities.country');

        $this->setQueryBuilder($queryBuilder);
    }

    /**
     * Add columns.
     *
     * @return void
     */
    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'city_id',
            'label'      => trans('city::app.datagrid.id'),
            'type'       => 'number',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'name',
            'label'      => trans('city::app.datagrid.name'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'country',
            'label'      => trans('city::app.datagrid.country'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
    }

    /**
     * Prepare actions.
     *
     * @return void
     */
    public function prepareActions()
    {
        $this->addAction([
            'method' => 'GET',
            'route'  => 'admin.city.edit',
            'icon'   => 'icon pencil-lg-icon',
            'title'  => trans('city::app.city.edit-help-title'),
        ]);

        $this->addAction([
            'method' => 'DELETE',
            'route'  => 'admin.city.destroy',
            'icon'   => 'icon trash-icon',
            'title'  => trans('city::app.city.delete-help-title'),
        ]);
    }
}
