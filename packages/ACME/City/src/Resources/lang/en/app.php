<?php

return [
    'city'      => [
        'name'              => 'Name',
        'country'           => 'Country',
        'title'             => 'Cities',
        'create-title'         => 'Add City',
        'edit-title'        => 'Edit City',
        'edit-help-title'   => 'Edit',
        'delete-help-title' => 'Delete',
        'save-btn-title'    => 'Save City',
        'enter'             => 'Enter',
        'create-btn-title'  => 'Save City',
        'edit-btn-title'    => 'Edit City'
    ],
    'datagrid'  => [
        'id'       => 'ID',
        'name'     => 'Name',
        'country'  => 'Country'
    ],
    'response' => [
        'create-success' => ':name created successfully.',
        'update-success'            => ':name updated successfully.',
        'delete-success'            => ':name deleted successfully.',
    ],
    'admin' => [
        'system' => [
            'custom-shipping-method' => 'Custom Shipping Method',
            'title' => 'Title'
        ]
    ]
];
