<div class="{{$errors->has($name) ? "control-group has-error": "control-group"}}">
    <label class="@if($required) required @endif">
        {{ __($item . '::app.'.$item. '.' .$name)}}
    </label>
    <input type="text" class="control"
           id="{{$name}}"
           placeholder="{{__($item . '::app.'. $item . '.enter')}} {{ __($item . '::app.'.$item. '.' .$name)}}"
           @if($required) required @endif
           name="{{ $name }}"
           value="{{ $oldValue ? $oldValue->{$name} : old($name) }}"
        {{ $readonly ? 'readonly' : '' }}
    />
    @error($name)
        <span class="control-error">{{ $message }}</span>
    @enderror
</div>