<div class="content">
    <form data-parsley-validate method="post" action="{{ $action ?? '#' }}" autocomplete="off" enctype="multipart/form-data" @submit.prevent="onSubmit">
        @csrf

        @if(isset($entity))
            @method('PUT')
        @endif

        <div class="page-header">
            <div class="page-title">
                <h1>
                    <i class="icon angle-left-icon back-link" onclick="window.location = '{{ route('admin.'. $item . '.index') }}'"></i>
                    {{ __($item . '::app.' . $item . '.title') }}
                </h1>
            </div>

            <div class="page-action">
                <button type="submit" class="btn btn-lg btn-primary">
                    {{ __($item . '::app.' . $item .  '.' .getAction() . '-btn-title') }}
                </button>
            </div>
        </div>

        @include($item . '::_form')

    </form>
</div>