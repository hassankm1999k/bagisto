@extends('admin::layouts.content')

@section('page_title')
    {{ __($item . '::app.'. $item . '.title') }}
@endsection

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __($item . '::app.' . $item . '.title') }}</h1>
            </div>
            <div class="page-action">
                <a href="{{ route('admin.' .$item . '.create') }}" class="btn btn-lg btn-primary">
                    {{ __($item . '::app.' . $item . '.create-title') }}
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('cityGrid','ACME\City\DataGrids\CityDataGrid')

            {!! $cityGrid->render() !!}
        </div>
    </div>

@endsection

@push('scripts')

@endpush

