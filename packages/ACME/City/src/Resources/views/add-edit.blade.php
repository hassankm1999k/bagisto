@extends('admin::layouts.content')

@section('page_title')
    {{ __($item . '::app.' . $item . '.' . getAction() . '-title') }}
@endsection

@section('content')
    <div class="page-content">
        <div class="form-container">
            <x-form :action="getNextAction($item, ${$item} ?? null)" :entity="${$item} ?? null" :submit="getActionMethod()"></x-form>
        </div>
    </div>
@endsection