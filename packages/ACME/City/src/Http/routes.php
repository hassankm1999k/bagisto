<?php
use Illuminate\Support\Facades\Route;

use ACME\City\Http\Controllers\CityController;

Route::group(['middleware' => ['admin', 'web'], 'prefix' => config('app.admin_url'), 'as' => 'admin.'], function (){
  Route::resource('city', CityController::class);
});
