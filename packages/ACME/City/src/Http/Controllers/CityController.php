<?php

namespace ACME\City\Http\Controllers;

use ACME\City\Http\Requests\CityRequest;
use ACME\City\Models\City;
use ACME\City\Repositories\CityRepository;

class CityController extends Controller
{
    public $resource = 'city';
    protected $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        view()->share('item', $this->resource);
        view()->share('class', City::class);
        $this->cityRepository = $cityRepository;
    }

    public function index()
    {
        return view($this->resource.'::index');
    }

    public function create()
    {
        return view($this->resource.'::add-edit');
    }

    public function store(CityRequest $request)
    {
        $this->cityRepository->store($request->all());

        session()->flash('success',
            trans($this->resource. '::app.response.create-success', ['name' => toTitle($this->resource)])
        );
        return redirect()->route('admin.' . $this->resource . '.index');
    }

    public function edit(City $city)
    {
        return view($this->resource.'::add-edit', compact('city'));
    }

    public function update(CityRequest $request, City $city)
    {
        $this->cityRepository->update($city->id, $request->all());

        session()->flash('success',
            trans($this->resource. '::app.response.update-success', ['name' => toTitle($this->resource)]));
        return redirect()->route('admin.' . $this->resource . '.index');
    }

    public function destroy($id)
    {
        $this->cityRepository->destroy($id);

        session()->flash('success',
            trans($this->resource. '::app.response.delete-success', ['name' => toTitle($this->resource)]));

        return redirect()->route('admin.' . $this->resource . '.index');
    }

}