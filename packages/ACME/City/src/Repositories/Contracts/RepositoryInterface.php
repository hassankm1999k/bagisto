<?php

namespace ACME\City\Repositories\Contracts;

interface RepositoryInterface
{
    public function index();
    public function getById($id);
    public function store(array $attributes);
    public function update($id, array $attributes);
    public function destroy($id);
}
