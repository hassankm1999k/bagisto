<?php

namespace ACME\City\Repositories;

use ACME\City\Models\City;

class CityRepository extends Repository
{
    protected $model;

    public function __construct(City $model)
    {
        $this->model = $model;
    }

}