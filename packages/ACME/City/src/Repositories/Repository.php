<?php

namespace ACME\City\Repositories;

use ACME\City\Repositories\Contracts\RepositoryInterface;

abstract class Repository implements RepositoryInterface
{
    public function index()
    {
        return $this->model->all();
    }

    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function store(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update($id, array $attributes)
    {
        $record = $this->getById($id);
        $record->update($attributes);
        return $record;
    }

    public function destroy($id)
    {
        $record = $this->getById($id);
        return $record->delete();
    }
}
